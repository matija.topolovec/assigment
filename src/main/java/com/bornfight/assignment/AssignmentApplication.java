package com.bornfight.assignment;

import com.bornfight.assignment.controllers.BattleController;
import com.bornfight.assignment.repositories.BattleRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class AssignmentApplication {

	BattleRepository battleRepository;

	public static void main(String[] args) {
		//SpringApplication.run(AssignmentApplication.class, args);
		ApplicationContext ctx = SpringApplication.run(AssignmentApplication.class, args);

		BattleController battleController = (BattleController) ctx.getBean("battleController");
	}

}
