package com.bornfight.assignment.converter;

import com.bornfight.assignment.dtos.ArmyDTO;
import com.bornfight.assignment.dtos.BattleDTO;
import com.bornfight.assignment.model.Battle;
import com.bornfight.assignment.services.ArmyService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Component
public class BattleDTOToBattle implements Converter<BattleDTO, Battle> {

    private final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("dd.MM.yyyy.");
    private final ArmyService armyService;
    private final ArmyToArmyDTO armyToArmyDTO;
    private final ArmyDTOToArmy armyDTOToArmy;

    public BattleDTOToBattle(ArmyService armyService, ArmyToArmyDTO armyToArmyDTO, ArmyDTOToArmy armyDTOToArmy) {
        this.armyService = armyService;
        this.armyToArmyDTO = armyToArmyDTO;
        this.armyDTOToArmy = armyDTOToArmy;
    }

    @Override
    public Battle convert(BattleDTO source) {

        if(source == null) {
            return null;
        }

        Battle battle = new Battle();
        battle.setId(source.getId());

        if(source.getArmyFirst() != null || source.getArmySecond() != null) {
            battle.setArmyFirst(source.getArmyFirst());
            battle.setArmySecond(source.getArmySecond());
        }

        battle.setBattleName(source.getBattleName());

        if(source.getFirstArmyName() != null || source.getSecondArmyName() != null) {
            ArmyDTO firstArmy = armyService.getArmyByName(source.getFirstArmyName());
            battle.setArmySetupFirst(armyDTOToArmy.convert(firstArmy));

            ArmyDTO secondArmy = armyService.getArmyByName(source.getSecondArmyName());
            battle.setArmySetupSecond(armyDTOToArmy.convert(secondArmy));
        }

        // Transform from String to LocalDate
        LocalDate battleDate = LocalDate.parse(source.getBattleDate(), DATE_FORMAT);
        battle.setBattleDate(battleDate);

        return battle;
    }
}
