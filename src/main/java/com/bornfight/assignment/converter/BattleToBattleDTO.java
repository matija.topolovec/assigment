package com.bornfight.assignment.converter;

import com.bornfight.assignment.dtos.BattleDTO;
import com.bornfight.assignment.model.Battle;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Component
public class BattleToBattleDTO implements Converter<Battle, BattleDTO> {

    private final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("dd.MM.yyyy.");

    @Override
    public BattleDTO convert(Battle source) {

        if(source == null) {
            return null;
        }

        BattleDTO battleDTO = new BattleDTO();
        battleDTO.setId(source.getId());
        battleDTO.setBattleName(source.getBattleName());
        battleDTO.setArmyFirst(source.getArmyFirst());
        battleDTO.setArmySecond(source.getArmySecond());

        if(source.getArmySetupFirst() != null || source.getArmySetupSecond() != null) {
            battleDTO.setArmySetupFirst(source.getArmySetupFirst());
            battleDTO.setArmySetupSecond(source.getArmySetupSecond());
            // Map From battle to battleDTO Selected Army names
            battleDTO.setFirstArmyName(source.getArmySetupFirst().getName());
            battleDTO.setSecondArmyName(source.getArmySetupSecond().getName());
        }

        // Setting from LocalDate to String
        LocalDate localDate = source.getBattleDate();
        battleDTO.setBattleDate(localDate.format(DATE_FORMAT));

        return battleDTO;
    }
}
