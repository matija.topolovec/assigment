package com.bornfight.assignment.converter;

import com.bornfight.assignment.dtos.ArmyDTO;
import com.bornfight.assignment.model.Army;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class ArmyDTOToArmy implements Converter<ArmyDTO, Army> {

    @Synchronized
    @Nullable
    @Override
    public Army convert(ArmyDTO source) {

        if(source == null) {
            return null;
        }

        final Army army = new Army();
        army.setId(source.getId());
        army.setName(source.getName());
        army.setGenerals(source.getGenerals());
        army.setOfficers(source.getOfficers());
        army.setSnipers(source.getSnipers());
        army.setSoldiers(source.getSoldiers());
        army.setTanks(source.getTanks());
        army.setAirFighters(source.getAirFighters());

        return army;
    }
}
