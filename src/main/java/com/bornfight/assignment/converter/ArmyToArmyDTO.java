package com.bornfight.assignment.converter;

import com.bornfight.assignment.dtos.ArmyDTO;
import com.bornfight.assignment.model.Army;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class ArmyToArmyDTO implements Converter<Army, ArmyDTO> {

    @Synchronized
    @Nullable
    @Override
    public ArmyDTO convert(Army source) {

        if(source == null) {
            return null;
        }

        ArmyDTO armyDTO = new ArmyDTO();
        armyDTO.setId(source.getId());
        armyDTO.setName(source.getName());
        armyDTO.setGenerals(source.getGenerals());
        armyDTO.setOfficers(source.getOfficers());
        armyDTO.setSnipers(source.getSnipers());
        armyDTO.setSoldiers(source.getSoldiers());
        armyDTO.setTanks(source.getTanks());
        armyDTO.setAirFighters(source.getAirFighters());

        return armyDTO;
    }
}
