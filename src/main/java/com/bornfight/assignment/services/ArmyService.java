package com.bornfight.assignment.services;

import com.bornfight.assignment.dtos.ArmyDTO;

import java.util.List;

public interface ArmyService {
    List<ArmyDTO> getAllArmies();
    ArmyDTO getArmyById(Long id);
    ArmyDTO addArmy(ArmyDTO army);
    ArmyDTO updateArmy(Long id, ArmyDTO army);
    void deleteArmy(Long id);
    ArmyDTO getArmyByName(String armyName);
}
