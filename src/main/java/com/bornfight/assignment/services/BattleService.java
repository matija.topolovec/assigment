package com.bornfight.assignment.services;

import com.bornfight.assignment.dtos.BattleDTO;

import java.util.List;

public interface BattleService {
    List<BattleDTO> getAllBattles();
    BattleDTO getBattleById(Long id);
    BattleDTO addBattle(BattleDTO battle);
    BattleDTO updateBattle(Long id, BattleDTO battle);
    void deleteBattle(Long id);
    BattleDTO getBattleByBattleName(String battleName);
    String getOutcome(Integer army1, Integer army2, String battleName);
}
