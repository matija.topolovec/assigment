package com.bornfight.assignment.services;

import com.bornfight.assignment.converter.ArmyDTOToArmy;
import com.bornfight.assignment.converter.ArmyToArmyDTO;
import com.bornfight.assignment.dtos.ArmyDTO;
import com.bornfight.assignment.exceptions.MethodNotAllowed;
import com.bornfight.assignment.exceptions.NotFoundException;
import com.bornfight.assignment.model.Army;
import com.bornfight.assignment.model.Battle;
import com.bornfight.assignment.repositories.ArmyRepository;
import com.bornfight.assignment.repositories.BattleRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ArmyServiceImp implements ArmyService {

    private final ArmyRepository armyRepository;
    private final BattleRepository battleRepository;
    private final ArmyToArmyDTO armyToArmyDTO;
    private final ArmyDTOToArmy armyDTOToArmy;

    public ArmyServiceImp(ArmyRepository armyRepository,
                          BattleRepository battleRepository,
                          ArmyToArmyDTO armyToArmyDTO,
                          ArmyDTOToArmy armyDTOToArmy) {
        this.armyRepository = armyRepository;
        this.battleRepository = battleRepository;
        this.armyToArmyDTO = armyToArmyDTO;
        this.armyDTOToArmy = armyDTOToArmy;
    }

    @Override
    public List<ArmyDTO> getAllArmies() {
        return armyRepository.findAll()
                .stream()
                .map(armyToArmyDTO::convert)
                .collect(Collectors.toList());
    }

    @Override
    public ArmyDTO getArmyById(Long id) {
        return armyRepository.findById(id)
                .map(armyToArmyDTO::convert)
                .orElseThrow(() -> new NotFoundException("There is no army with this Id: " + id + " in the database"));
    }

    @Override
    public ArmyDTO addArmy(ArmyDTO army) {
        if(armyRepository.findArmyByNameEquals(army.getName()).isPresent()) {
            throw new MethodNotAllowed("There is allready one army with that name in database");
        }
        Army savedArmy = armyRepository.save(armyDTOToArmy.convert(army));
        return armyToArmyDTO.convert(savedArmy);
    }

    @Override
    public ArmyDTO updateArmy(Long id, ArmyDTO army) {
        Army updatedArmy;
        if(armyRepository.existsById(id)) {
            updatedArmy = armyDTOToArmy.convert(army);
            updatedArmy.setId(id);
        } else {
            throw new NotFoundException("There is no army with that id!");
        }
        armyRepository.saveAndFlush(updatedArmy);
        return armyToArmyDTO.convert(updatedArmy);
    }

    @Override
    public void deleteArmy(Long id) {
        Army armyToDelete = armyRepository.getOne(id);
        Optional<Battle> battle = battleRepository.getBattleByArmySetupFirstOrAndArmySetupSecond(armyToDelete, armyToDelete);

        if(battle.isPresent()) {
            throw new MethodNotAllowed("You can't delete this army. It is on Battle");
        }
        if(armyRepository.existsById(id)) {
            armyRepository.deleteById(id);
        } else {
            throw new NotFoundException("There is no army with that id!");
        }
    }

    @Override
    public ArmyDTO getArmyByName(String armyName) {
        return armyRepository.findArmyByNameEquals(armyName)
                .map(armyToArmyDTO::convert)
                .orElseThrow(() -> new NotFoundException("There is no army with army name: " + armyName + " in the database"));
    }
}
