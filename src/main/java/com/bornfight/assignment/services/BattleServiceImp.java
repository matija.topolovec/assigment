package com.bornfight.assignment.services;

import com.bornfight.assignment.converter.BattleDTOToBattle;
import com.bornfight.assignment.converter.BattleToBattleDTO;
import com.bornfight.assignment.dtos.BattleDTO;
import com.bornfight.assignment.enums.DailyWeatherConditions;
import com.bornfight.assignment.enums.Seasons;
import com.bornfight.assignment.exceptions.MethodNotAllowed;
import com.bornfight.assignment.exceptions.NotFoundException;
import com.bornfight.assignment.model.Army;
import com.bornfight.assignment.model.Battle;
import com.bornfight.assignment.repositories.BattleRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Slf4j
@Service
public class BattleServiceImp implements BattleService {

    private final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("dd.MM.yyyy.");

    private final BattleRepository battleRepository;
    private final BattleToBattleDTO battleToBattleDTO;
    private final BattleDTOToBattle battleDTOToBattle;

    public BattleServiceImp(BattleRepository battleRepository, BattleToBattleDTO battleToBattleDTO, BattleDTOToBattle battleDTOToBattle) {
        this.battleRepository = battleRepository;
        this.battleToBattleDTO = battleToBattleDTO;
        this.battleDTOToBattle = battleDTOToBattle;
    }

    @Override
    public List<BattleDTO> getAllBattles() {
        return battleRepository.findAll()
                .stream()
                .map(battleToBattleDTO::convert)
                .collect(Collectors.toList());
    }

    @Override
    public BattleDTO getBattleById(Long id) {
        return battleRepository.findById(id)
                .map(battleToBattleDTO::convert)
                .orElseThrow(() -> new NotFoundException("There is no battle with this Id:" + id + " in database!"));
    }

    @Override
    public BattleDTO addBattle(BattleDTO battle) {
        if(battleRepository.getBattleByBattleName(battle.getBattleName()).isPresent()) {
            throw new MethodNotAllowed("There is allready one battle with that name in database");
        }
        Battle savedBattle = battleRepository.save(battleDTOToBattle.convert(battle));
        return battleToBattleDTO.convert(savedBattle);
    }

    @Override
    public BattleDTO updateBattle(Long id, BattleDTO battle) {
        Battle updatedBattle;
        if(battleRepository.existsById(id)) {
            updatedBattle = battleDTOToBattle.convert(battle);
            updatedBattle.setId(id);
        } else {
            throw new NotFoundException("There is no battle with that id!");
        }
        battleRepository.saveAndFlush(updatedBattle);
        return battleToBattleDTO.convert(updatedBattle);
    }

    @Override
    public void deleteBattle(Long id) {
        if(battleRepository.existsById(id)) {
            battleRepository.deleteById(id);
        } else {
            throw new NotFoundException("There is no battle with that id!");
        }
    }

    @Override
    public BattleDTO getBattleByBattleName(String battleName) {
        return battleRepository.getBattleByBattleName(battleName)
                .map(battleToBattleDTO::convert)
                .orElseThrow(() -> new NotFoundException("There is no battle with battlename: " + battleName + " in the database"));
    }
    @Override
    public String getOutcome(Integer army1, Integer army2, String battleName) {

        BattleDTO battle = battleRepository.getBattleByBattleName(battleName)
                .map(battleToBattleDTO::convert)
                .orElseThrow(() -> new NotFoundException("There is no battle with battlename: " + battleName + " in the database"));
        battle.setArmyFirst(army1);
        battle.setArmySecond(army2);

        battleRepository.save(battleDTOToBattle.convert(battle));

        log.debug("Getting outcome of the fight");

        Army strongerArmy = getStrongerArmyByArmySetup(battle);
        log.debug("Stronger armyName is:  " + strongerArmy.getName());

        if(army1 > army2 && battle.getArmySetupFirst().getName().equals(strongerArmy.getName())) {
            log.debug("ArmyOne wins the battle!");
            return "ArmyOne wins the battle!";
        } else if(army1 > army2 && battle.getArmySetupSecond().getName().equals(strongerArmy.getName())){
            log.debug("ArmyTwo wins the battle!");
            return "ArmyTwo wins the battle!";
        } else if(army2 > army1 && battle.getArmySetupSecond().getName().equals(strongerArmy.getName())){
            log.debug("ArmyTwo wins the battle!");
            return "ArmyTwo wins the battle!";
        } else if(army2 > army1 && battle.getArmySetupFirst().getName().equals(strongerArmy.getName())){
            log.debug("ArmyOne wins the battle!");
            return "ArmyOne wins the battle!";
        } else if (army1 == army2 && battle.getArmySetupFirst().getName().equals(strongerArmy.getName())){
            log.debug("ArmyOne wins the battle!");
            return "ArmyOne wins the battle!";
        } else if (army1 == army2 && battle.getArmySetupSecond().getName().equals(strongerArmy.getName())) {
            log.debug("ArmyTwo wins the battle!");
            return "ArmyTwo wins the battle!";
        } else {
            log.debug("Armies are equal, it is a draw");
            return "Armies are equal so it is a draw!";
        }
    }

    private Army getStrongerArmyByArmySetup(BattleDTO battle) {
        Army armyFirst = battle.getArmySetupFirst();
        Army armySecond = battle.getArmySetupSecond();

        String season = getSeason(battleDTOToBattle.convert(battle));

        log.debug(season);

        return getArmyStrongerCountOfSoldiers(armyFirst, armySecond, season);
    }

    private Army getArmyStrongerCountOfSoldiers(Army armyFirst, Army armySecond, String season) {

        Double armyFirstPoints = 1.0;
        Double armySecondPoints = 1.0;

        Double weatherFactor = getWeatherFactor(season);
        log.debug("Weather factor is: " + weatherFactor + " Condition: " + DailyWeatherConditions.valueOfFactor(weatherFactor));

        List<Double> points = compareSoldiersByRanks(armyFirstPoints, armySecondPoints, armyFirst.getGenerals(), armySecond.getGenerals(), weatherFactor);
        armyFirstPoints = points.get(0);
        armySecondPoints = points.get(1);

        log.debug("Points after generals - First: " + armyFirstPoints + " Second: " + armySecondPoints);

        points = compareSoldiersByRanks(armyFirstPoints, armySecondPoints, armyFirst.getOfficers(), armySecond.getOfficers(), weatherFactor);
        armyFirstPoints = points.get(0);
        armySecondPoints = points.get(1);

        log.debug("Points after officers - First: " + armyFirstPoints + " Second: " + armySecondPoints);

        points = compareSoldiersByRanks(armyFirstPoints, armySecondPoints, armyFirst.getSnipers(), armySecond.getSnipers(), weatherFactor);
        armyFirstPoints = points.get(0);
        armySecondPoints = points.get(1);

        log.debug("Points after snipers - First: " + armyFirstPoints + " Second: " + armySecondPoints);

        points = compareSoldiersByRanks(armyFirstPoints, armySecondPoints, armyFirst.getSoldiers(), armySecond.getSoldiers(), weatherFactor);
        armyFirstPoints = points.get(0);
        armySecondPoints = points.get(1);

        log.debug("Points after soldiers - First: " + armyFirstPoints + " Second: " + armySecondPoints);

        points = compareSoldiersByRanks(armyFirstPoints, armySecondPoints, armyFirst.getTanks(), armySecond.getTanks(), weatherFactor);
        armyFirstPoints = points.get(0);
        armySecondPoints = points.get(1);

        log.debug("Points after tanks - First: " + armyFirstPoints + " Second: " + armySecondPoints);

        points = compareSoldiersByRanks(armyFirstPoints, armySecondPoints, armyFirst.getAirFighters(), armySecond.getAirFighters(), weatherFactor);
        armyFirstPoints = points.get(0);
        armySecondPoints = points.get(1);

        log.debug("Total Points after AirFighters - First: " + armyFirstPoints + " Second: " + armySecondPoints);

        if(Double.compare(armyFirstPoints, armySecondPoints) > 0) {
            return armyFirst;
        } else if (Double.compare(armyFirstPoints, armySecondPoints) < 0) {
            return armySecond;
        } else {
            Integer random = new Random().nextInt(2);

            if(random == 0) {
                return armyFirst;
            } else {
                return armySecond;
            }
        }
    }

    private Integer seasonToInteger(String season) {

        Integer seasonNumber = 0;

        if(season.equals(Seasons.WINTER.toString())) {
            seasonNumber = 0;
        } else if(season.equals(Seasons.SPRING.toString())) {
            seasonNumber = 1;
        } else if(season.equals(Seasons.SUMMER.toString())) {
            seasonNumber = 2;
        } else if(season.equals(Seasons.AUTUMN.toString())) {
            seasonNumber = 3;
        }

        return seasonNumber;
    }

    private String getSeason(Battle battle) {

        String season = "";

        // Season dates
        LocalDate winterDate = LocalDate.parse("21.12." + battle.getBattleDate().getYear() + ".", DATE_FORMAT);
        LocalDate springDate = LocalDate.parse("20.03." + battle.getBattleDate().getYear() + ".", DATE_FORMAT);
        LocalDate summerDate = LocalDate.parse("21.06." + battle.getBattleDate().getYear() + ".", DATE_FORMAT);
        LocalDate autumnDate = LocalDate.parse("22.09." + battle.getBattleDate().getYear() + ".", DATE_FORMAT);

        if((battle.getBattleDate().isAfter(winterDate) && battle.getBattleDate().isBefore(LocalDate.parse("31.12." + battle.getBattleDate().getYear() + ".", DATE_FORMAT)))
                || battle.getBattleDate().isBefore(springDate)
                || battle.getBattleDate().format(DATE_FORMAT).equals("21.12." + battle.getBattleDate().getYear() + ".")) {
            season = Seasons.WINTER.toString();
        } else if((battle.getBattleDate().isAfter(springDate) && battle.getBattleDate().isBefore(summerDate))
                || battle.getBattleDate().format(DATE_FORMAT).equals("20.03." + battle.getBattleDate().getYear() + ".")) {
            season = Seasons.SPRING.toString();
        } else if((battle.getBattleDate().isAfter(summerDate) && battle.getBattleDate().isBefore(autumnDate))
                || battle.getBattleDate().format(DATE_FORMAT).equals("21.06." + battle.getBattleDate().getYear() + ".")) {
            season = Seasons.SUMMER.toString();
        } else if ((battle.getBattleDate().isAfter(autumnDate) && battle.getBattleDate().isBefore(winterDate))
                || battle.getBattleDate().format(DATE_FORMAT).equals("22.09." + battle.getBattleDate().getYear() + ".")) {
            season = Seasons.AUTUMN.toString();
        } else {
            log.debug("Something went wrong!");
        }
        return season;
    }

    private Double getWeatherFactor(String season) {

        Double getDailyWeather = 0.0;

        switch (seasonToInteger(season)) {
            // Winter
            case 0:
                getDailyWeather = getDailyWeatherForWinter();
                break;
            // Spring
            case 1:
                getDailyWeather = getDailyWeatherForSpring();
                break;
            // Summer
            case 2:
                getDailyWeather = getDailyWeatherForSummer();
                break;
            // Autumn
            case 3:
                getDailyWeather = getDailyWeatherForAutumn();
                break;
        }

        return getDailyWeather;
    }

    private Double getDailyWeatherForWinter() {

        Double factor = 0.0;

        Integer random = new Random().nextInt(5);

        switch(random) {
            case 0:
                // Sunny day
                factor = DailyWeatherConditions.valueOfLabel("Sunny day").factor;
                break;
            case 1:
                // Snowy weather
                factor = DailyWeatherConditions.valueOfLabel("Snowy day").factor;
                break;
            case 2:
                // Really cold day
                factor = DailyWeatherConditions.valueOfLabel("Really cold day").factor;
                break;
            case 3:
                // Cloudy day
                factor = DailyWeatherConditions.valueOfLabel("Cloudy day").factor;
                break;
            case 4:
                // Extreme weather conditions
                factor = DailyWeatherConditions.valueOfLabel("Extreme weather conditions").factor;
                break;
        }
        return factor;
    }

    private Double getDailyWeatherForSpring() {
        Double factor = 0.0;

        Integer random = new Random().nextInt(5);

        switch(random) {
            case 0:
                // Sunny day
                factor = DailyWeatherConditions.valueOfLabel("Sunny day").factor;
                break;
            case 1:
                // Rainy day
                factor = DailyWeatherConditions.valueOfLabel("Rainy day").factor;
                break;
            case 2:
                // Cloudy day
                factor = DailyWeatherConditions.valueOfLabel("Cloudy day").factor;
                break;
            case 3:
                // Flood
                factor = DailyWeatherConditions.valueOfLabel("Water everywhere").factor;
                break;
            case 4:
                // Extreme weather conditions
                factor = DailyWeatherConditions.valueOfLabel("Extreme weather conditions").factor;
                break;
        }
        return factor;
    }

    private Double getDailyWeatherForSummer() {
        Double factor = 0.0;

        Integer random = new Random().nextInt(5);

        switch(random) {
            case 0:
                // Sunny day
                factor = DailyWeatherConditions.valueOfLabel("Sunny day").factor;
                break;
            case 1:
                // Rainy day
                factor = DailyWeatherConditions.valueOfLabel("Rainy day").factor;
                break;
            case 2:
                // Really hot day
                factor = DailyWeatherConditions.valueOfLabel("Really hot day").factor;
                break;
            case 3:
                // Cloudy day
                factor = DailyWeatherConditions.valueOfLabel("Cloudy day").factor;
                break;
            case 4:
                // Extreme weather conditions
                factor = DailyWeatherConditions.valueOfLabel("Extreme weather conditions").factor;
                break;
        }
        return factor;
    }

    private Double getDailyWeatherForAutumn() {
        Double factor = 0.0;

        Integer random = new Random().nextInt(5);

        switch(random) {
            case 0:
                // Sunny day
                factor = DailyWeatherConditions.valueOfLabel("Sunny day").factor;
                break;
            case 1:
                // Rainy day
                factor = DailyWeatherConditions.valueOfLabel("Rainy day").factor;
                break;
            case 2:
                // Foggy day
                factor = DailyWeatherConditions.valueOfLabel("Foggy day").factor;
                break;
            case 3:
                // Cloudy day
                factor = DailyWeatherConditions.valueOfLabel("Cloudy day").factor;
                break;
            case 4:
                // Extreme weather conditions
                factor = DailyWeatherConditions.valueOfLabel("Extreme weather conditions").factor;
                break;
        }
        return factor;
    }

    private List<Double> compareSoldiersByRanks(Double armyFirstPoints, Double armySecondPoints, Integer armyFirstClass, Integer armySecondClass, Double weatherFactor) {

        List<Double> result = new ArrayList<>();

        // Compare Soldiers numbers by their class if it is equal randomly get army points!
        if(armyFirstClass > armySecondClass) {
            armyFirstPoints += 1.0;
            armySecondPoints *= weatherFactor;
            log.debug("I have just used multiply on armySecondPoints by: " + weatherFactor);
        } else if(armyFirstClass < armySecondClass) {
            armySecondPoints += 1.0;
            armyFirstPoints *= weatherFactor;
            log.debug("I have just used multiply on armyFirstPoints by: " + weatherFactor);
        } else {
            // Random number from 0-1
            armyFirstPoints += new Random().nextInt(2);
            armySecondPoints += new Random().nextInt(2);
        }
        result.add(0,armyFirstPoints);
        result.add(1,armySecondPoints);
        return result;
    }

}
