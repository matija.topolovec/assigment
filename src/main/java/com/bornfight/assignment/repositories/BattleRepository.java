package com.bornfight.assignment.repositories;

import com.bornfight.assignment.model.Army;
import com.bornfight.assignment.model.Battle;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BattleRepository extends JpaRepository<Battle, Long> {
    Optional<Battle> getBattleByBattleName(String battleName);
    Optional<Battle> getBattleByArmySetupFirstOrAndArmySetupSecond(Army armyFirst, Army armySecond);
}
