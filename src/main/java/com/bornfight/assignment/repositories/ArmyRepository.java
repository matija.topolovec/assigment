package com.bornfight.assignment.repositories;

import com.bornfight.assignment.model.Army;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ArmyRepository extends JpaRepository<Army, Long> {
    Optional<Army> findArmyByNameEquals(String armyName);
}
