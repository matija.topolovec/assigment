package com.bornfight.assignment.bootstrap;

import com.bornfight.assignment.repositories.BattleRepository;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class BattleBootstrap {

    private final BattleRepository battleRepository;

    public BattleBootstrap(BattleRepository battleRepository) {
        this.battleRepository = battleRepository;
    }

    @PostConstruct
    public void onApplicationStart() {

        // Army
        /*
        Army armyOne = new Army();
        armyOne.setName("First army");
        armyOne.setGenerals(3);
        armyOne.setOfficers(5);
        armyOne.setSnipers(1);
        armyOne.setSoldiers(10);
        armyOne.setTanks(2);
        armyOne.setAirFighters(1);
        */
        /*
        Army armyTwo = new Army();
        armyOne.setName("Second army");
        armyOne.setGenerals(5);
        armyOne.setOfficers(4);
        armyOne.setSnipers(2);
        armyOne.setSoldiers(5);
        armyOne.setTanks(1);
        armyOne.setAirFighters(2);
        */
        // Battle
        /*
        Battle worldWarOne = new Battle();
        worldWarOne.setArmyFirst(58);
        worldWarOne.setArmySecond(68);
        worldWarOne.setBattleOutcome("ArmyTwo wins the battle!");
        battleRepository.save(worldWarOne);
         */
    }
}
