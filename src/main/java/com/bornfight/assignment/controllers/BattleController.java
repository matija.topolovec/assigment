package com.bornfight.assignment.controllers;

import com.bornfight.assignment.dtos.BattleDTO;
import com.bornfight.assignment.services.BattleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/battles")
public class BattleController {

    private final BattleService battleService;

    public BattleController(BattleService battleService) {
        this.battleService = battleService;
    }

    @GetMapping
    public List<BattleDTO> getBattles() {
        log.debug("Get all battles");
        return battleService.getAllBattles();
    }

    @GetMapping("/{id}")
    public ResponseEntity<BattleDTO> getBattle(@PathVariable("id") final Long id) {
        BattleDTO battle = battleService.getBattleById(id);
        log.debug("Get battle with id " + id);
        return new ResponseEntity<>(battle, HttpStatus.OK);
    }

    @PostMapping("/new")
    public ResponseEntity<String> addBattle(@Valid @RequestBody BattleDTO battle) {

        battleService.addBattle(battle);
        log.debug("Adding new battle");

        String response = "Battle with battle name: " + battle.getBattleName() + " created Successfully";
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<BattleDTO> updateBattle(@PathVariable("id") final Long id,
                                               @Valid @RequestBody final BattleDTO battle) {
        BattleDTO updatedBattle = battleService.updateBattle(id, battle);
        log.debug("Updating battle with id " + id);

        return new ResponseEntity<>(updatedBattle, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteBattle(@PathVariable("id") final Long id) {
        battleService.deleteBattle(id);
        log.debug("Deleted battle with id " + id);
        return new ResponseEntity<>("Successfully deleted battle.", HttpStatus.OK);
    }

    @RequestMapping("/data")
    public String getBattleOutcome(@RequestParam("army1") Integer army1,
                                   @RequestParam("army2") Integer army2,
                                   @RequestParam("battleName") String battleName) {

        log.debug("Got GET request of army outcome. Army1 = " + army1 + " Army2 = " + army2 + " with battleName = " + battleName);
        return battleService.getOutcome(army1, army2, battleName);

    }
}
