package com.bornfight.assignment.controllers;

import com.bornfight.assignment.dtos.ArmyDTO;
import com.bornfight.assignment.services.ArmyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/armies")
public class ArmyController {
    private final ArmyService armyService;

    public ArmyController(ArmyService armyService) {
        this.armyService = armyService;
    }

    @GetMapping
    public List<ArmyDTO> allArmies() {
        log.debug("Getting all armies");
        return armyService.getAllArmies();
    }

    @GetMapping("/{id}")
    public ResponseEntity<ArmyDTO> getArmy(@PathVariable("id") final Long id) {
        ArmyDTO army = armyService.getArmyById(id);
        log.debug("Getting army with id of " + id);
        return new ResponseEntity<>(army, HttpStatus.OK);
    }

    @PostMapping("/new")
    public ResponseEntity<ArmyDTO> addArmy(@Valid @RequestBody final ArmyDTO army) {
        ArmyDTO armyToSave = armyService.addArmy(army);
        log.debug("Adding new army to database");
        return new ResponseEntity<>(armyToSave, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ArmyDTO> updateArmy(@PathVariable("id") final Long id,
                                              @Valid @RequestBody final ArmyDTO army) {
        ArmyDTO updatedArmy = armyService.updateArmy(id, army);
        log.debug("Updating army with id of " + id);
        return new ResponseEntity<>(updatedArmy, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteArmy(@PathVariable("id") final Long id) {
        armyService.deleteArmy(id);
        log.debug("Deleted army with id of " + id);
        return new ResponseEntity<>("Successfully deleted army.", HttpStatus.OK);
    }

}
