package com.bornfight.assignment.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class Army {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private Integer generals;
    private Integer officers;
    private Integer snipers;
    private Integer soldiers;

    private Integer tanks;
    private Integer airFighters;
}
