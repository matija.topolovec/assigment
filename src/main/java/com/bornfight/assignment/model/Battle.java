package com.bornfight.assignment.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class Battle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer armyFirst;
    private Integer armySecond;
    private String battleName;

    @ManyToOne
    private Army armySetupFirst;

    @ManyToOne
    private Army armySetupSecond;

    private LocalDate battleDate;
    private String battleOutcome;
}
