package com.bornfight.assignment.dtos;

import com.bornfight.assignment.model.Army;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
public class BattleDTO {

    @JsonIgnore
    private Long id;

    @JsonIgnore
    private Integer armyFirst;

    @JsonIgnore
    private Integer armySecond;

    @JsonIgnore
    private Army armySetupFirst;

    @JsonIgnore
    private Army armySetupSecond;

    @NotBlank(message = "Battle name cannot be blank!")
    private String battleName;

    @NotBlank(message = "First Army name cannot be blank!")
    private String firstArmyName;

    @NotBlank(message = "Second Army name cannot be blank!")
    private String secondArmyName;

    @Pattern(regexp="^((0?[1-9]|[12][0-9]|3[01]).(0?[1-9]|1[012]).((19|20)[0-9][0-9]).)",
            message = "Must be valid date with pattern 01.01.2000. and years from 1900. - 2099.")
    private String battleDate;

}
