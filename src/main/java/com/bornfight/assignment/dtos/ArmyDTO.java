package com.bornfight.assignment.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Data
public class ArmyDTO {

    @JsonIgnore
    private Long id;

    @NotBlank(message = "Army name cannot be blank!")
    private String name;

    @Min(value = 0, message = "The amount of Generals should be 0 or more")
    @Max(value = 100, message = "The amount of Generals should be maximum of 100")
    private Integer generals;
    @Min(value = 0, message = "The amount of Officers should be 0 or more")
    @Max(value = 100, message = "The amount of Officers should be maximum of 100")
    private Integer officers;
    @Min(value = 0, message = "The amount of Snipers should be 0 or more")
    @Max(value = 100, message = "The amount of Snipers should be maximum of 100")
    private Integer snipers;
    @Min(value = 0, message = "The amount of Soldiers should be 0 or more")
    @Max(value = 100, message = "The amount of Soldiers should be maximum of 100")
    private Integer soldiers;

    @Min(value = 0, message = "The amount of Tanks should be 0 or more")
    @Max(value = 100, message = "The amount of Tanks should be maximum of 100")
    private Integer tanks;
    @Min(value = 0, message = "The amount of Air Fighters should be 0 or more")
    @Max(value = 100, message = "The amount of Air Fighters should be maximum of 100")
    private Integer airFighters;
}
