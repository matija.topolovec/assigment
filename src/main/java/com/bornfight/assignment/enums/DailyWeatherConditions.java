package com.bornfight.assignment.enums;

import java.util.HashMap;
import java.util.Map;

public enum DailyWeatherConditions {

    SUN("Sunny day", 1.0),
    RAIN("Rainy day", 0.8),
    HEAVY_RAIN("Heavy rain", 0.6),
    HOT("Really hot day", 0.8),
    COLD("Really cold day", 0.8),
    FOG("Foggy day", 0.6),
    CLOUDY("Cloudy day", 0.9),
    SNOW("Snowy day", 0.4),
    MUDDY("Mud everywhere", 0.1),
    FLOODING("Water everywhere", 0.1),
    EXTREME_WEATHER("Extreme weather conditions", 0.01);

    private static final Map<String, DailyWeatherConditions> BY_LABEL = new HashMap<>();
    private static final Map<Double, DailyWeatherConditions> BY_FACTOR = new HashMap<>();

    static {
        for (DailyWeatherConditions d : values()) {
            BY_LABEL.put(d.label, d);
            BY_FACTOR.put(d.factor, d);
        }
    }

    public final String label;
    public final Double factor;

    DailyWeatherConditions(String label, Double factor) {
        this.label = label;
        this.factor = factor;
    }

    public static DailyWeatherConditions valueOfLabel(String label) {
        return BY_LABEL.get(label);
    }

    public static DailyWeatherConditions valueOfFactor(Double factor) {
        return BY_FACTOR.get(factor);
    }

}
